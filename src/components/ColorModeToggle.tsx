import * as React from "react";
import { useColorMode, Button } from "theme-ui";

const modes = [
  "default", "dark"//, "deep", "swiss"
];

const ColorModeToggle: React.FC = () => {
  const [colorMode, setColorMode] = useColorMode();

  const cycleColorMode = () => {
    const i = modes.indexOf(colorMode);
    const next = modes[(i + 1) % modes.length];
    setColorMode(next);
  };

  return (
    <Button onClick={cycleColorMode}>
      <b>{colorMode}</b>
    </Button>
  );
};

export default ColorModeToggle;
