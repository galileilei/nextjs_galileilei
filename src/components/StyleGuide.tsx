/** @jsxRuntime classic */
/** @jsx jsx */
import * as React from "react";
import { jsx, useColorMode,  SxProp } from "theme-ui";
import theme from "../constants/theme";

const StyleGuide: React.FC<SxProp> = () => {
  const [colorMode, setColorMode] = useColorMode();
  return (
    <>{/* @ts-ignore */}
      <pre sx={{ color: "secondary" }}>
        style is {JSON.stringify(theme, null, "\t")}
      </pre>
    </>
  );
};

export default StyleGuide;
