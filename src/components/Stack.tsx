import * as React from 'react'
import { Box } from 'theme-ui'

const Stack: React.FC = ({
	children,
}) => {
	const items = React.Children.toArray(children)

	return (
		<Box>
			{items.map((child, index) => (
				<Box
					key={index}
				>
					{child}
				</Box>
			))}
		</Box>
	)
}

export default Stack
