/**@jsxRuntime classic */
/**@jsx jsx */
import { jsx, Box, Flex } from "theme-ui";
import * as React from "react";
import Link, { NavLinkProps } from "./Link";
import ColorModeToggle from "./ColorModeToggle";

const links: NavLinkProps[] = [
  { dest: "/", text: "About" },
  { dest: "/resume", text: "Résumé" },
  { dest: "/projects", text: "Projects" },
  { dest: "/notes", text: "Notes" },
];

const Nav: React.FC = () => {
  return (
    <Box as="header">
      <Flex as="nav"> 
        <Box sx={{ mx: "auto", display: "inline", flex: "1 1 auto" }}/>
        {links.map((props) => (
          <Link key={props.dest} underlined {...props} />
        ))}
        <ColorModeToggle />
      </Flex>
    </Box>
  );
};

export default Nav;
