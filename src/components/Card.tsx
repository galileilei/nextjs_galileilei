import * as React from "react";
import { Flex, Box, Heading } from "theme-ui";
import { Link } from "./Link";
import { Project } from "../lib/utils";

const Card: React.FC<Project> = ({ link, meta }) => {
  return (
    <Flex
      as="article"
      mx={1}
      p={[1,2]}
      sx={{ flex: "1 1 auto", flexDirection: "column" }}
      variant="styles.card"
    >
      <Heading as="h5" mb={2} bg="muted">
        {meta.date}
      </Heading>
      <Link href={link}>
        <Heading mb={2}>{meta.title}</Heading>
      <Heading as="h3" mb={2}>
        {meta.subtitle}
      </Heading>
      </Link>
      <Box as="ul">
        {meta.description && meta.description.map((x) => (
          <li key={x}>{x}</li>
        ))}
      </Box>
      <Flex sx={{ flexDirection: "row" }}>
        {meta.links && Object.keys(meta.links).map((key) => {
          return (
            <Link key={key} href={meta.links[key]} mx={[1, 2]}>
              {key}
            </Link>
          );
        })}
        <Box sx={{ mx: "auto" }}></Box>
      </Flex>
    </Flex>
  );
};

export default Card;
