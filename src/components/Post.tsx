import * as React from "react";
import { Box, Container } from "theme-ui";
import Head from "next/head";
import Stack from "./Stack";
import { Meta } from "../lib/utils";

interface Props {
  meta: Meta;
  // children: React.ReactNode;
}

const Post: React.FC<Props> = ({ meta, children }) => {
  return (
    <React.Fragment>
      <Head>
        <title key="title">{meta.title} </title>
        <meta name="description" content={meta.title} />
      </Head>
      <Box as="header">
        debug: date is {meta.date} and title is {meta.title}
      </Box>

      <Container variant="post">
        <Stack>{children}</Stack>
      </Container>
    </React.Fragment>
  );
};

export default Post;
