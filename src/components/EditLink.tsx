import * as React from "react";
import { useRouter } from "next/router";

const getHREF = (base: string, path: string) => {
  const sanitizePath = path.replace(/\/+$/, "");
  const href = `${base}` + sanitizePath;

  if (path === "/") return `${href}/index.mdx`;
  if (path === "/projects") return `${href}.tsx`;
  if (path === "/notes") return `${href}.tsx`;
  return `${href}.mdx`;
};

interface EditLinkProps {
  base?: string;
  children?: string;
}

const EditLink: React.FC<EditLinkProps> = (props) => {
  const {
    base = "https://bitbucket.org/galileilei/nextjs_galileilei/src/master/src/pages",
    children = "View source on Bitbucket",
  } = props;

  const { pathname } = useRouter();
  const href = getHREF(base, pathname);

  return <a href={href}> {children} </a>;
};

export default EditLink;
