import { NavLink as ThemeLink, Link as Anchor, SxProp } from "theme-ui";
import NextLink from "next/link";
import { useRouter } from "next/router";

export interface NavLinkProps extends SxProp {
  dest: string;
  text: string;
  underlined?: boolean;
}

const NavLink: React.FC<NavLinkProps> = ({
  dest,
  text,
  underlined = false,
}) => {
  const { pathname } = useRouter();

  const checkPath = () => {
    if (dest === "/") return pathname === "/" ? true : false;
    return pathname.includes(dest) ? true : false;
  };
  return (
    <NextLink href={dest} passHref>
      <ThemeLink
        href={dest}
        data-active={underlined && checkPath() ? true : null}
      >
        {text}
      </ThemeLink>
    </NextLink>
  );
};

interface LinkProps extends SxProp {
  href: string;
  mx?: number[];
}

export const Link: React.FC<LinkProps> = ({ href, ...props }) => (
  <NextLink href={href} passHref>
    <Anchor {...props} />
  </NextLink>
);

export default NavLink;
