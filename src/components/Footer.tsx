/**@jsxRuntime classic */
/**@jsx jsx */
import * as React from "react";
import Link, { NavLinkProps } from "./Link";
import { jsx, Box } from "theme-ui";

const links: NavLinkProps[] = [
  { dest: "/", text: "Home" },
  { dest: "/notes", text: "Notes" },
  { dest: "/projects", text: "Projects" },
  { dest: "/resume", text: "Résumé" },
  { dest: "https://bitbucket.org/galileilei/", text: "Bitbucket" },
  { dest: "https://www.linkedin.com/in/lei-lei-1001/", text: "Linkedin" },
];

const Footer: React.FC = () => {
  return (
    <Box
      as="footer"
      sx={{
        display: "grid",
        gridTemplateRows: "repeat(3, 48px)",
        gridTemplateColumns: ["1fr", "1fr 1fr 3fr"],
        gridAutoFlow: "column",
        px: 2,
        py: 4,
      }}
    >
      {links.map((props) => (
        <Link key={props.text} {...props} />
      ))}
    </Box>
  );
};

export default Footer;
