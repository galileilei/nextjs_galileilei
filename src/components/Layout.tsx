/** @jsxRuntime classic */
/** @jsx jsx */
import * as React from "react";
import metadata from "../constants/metadata.json";
import Head from "next/head";
import { jsx, Flex, Box } from "theme-ui";
import Nav from "./Nav";
import Footer from "./Footer";
import EditLink from "./EditLink";

const Layout: React.FC = ({ children }) => {
  const { title, description } = metadata;

  return (
    <React.Fragment>
      <Head>
        {/* https://developer.mozilla.org/en-US/docs/Web/HTML/Viewport_meta_tag#viewport_basics */}
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title key="title">{title}</title>
        <meta name="description" content={description} />
      </Head>

      <Nav />
      <Flex
        as="main"
        sx={{ width: "100%", flex: "1 1 auto", flexDirection: "column" }}
      >
        {children}
        <Box>
          <EditLink />
        </Box>
      </Flex>
      <Footer />
    </React.Fragment>
  );
};

export default Layout;
