// no idea how this function works, taken from
// lowmess
/* eslint-disable import/exports-last */
const importAll = (r: __WebpackModuleApi.RequireContext, prefix: string) => {
  return r.keys().map((fileName) => ({
    link: `${prefix}${fileName.substring(1).replace(/\.mdx$/, "")}`,
    meta: r(fileName).meta,
  }));
};

export type Meta = {
  title: string;
  subtitle?: string;
  date?: string;
  description?: string[];
  links?: { [name: string]: string };
  show?: boolean;
};

export type Project = {
  link: string;
  meta: Meta;
};

export const projects = importAll(require.context("../pages/projects/", true, /\.mdx$/), "/projects");
export const notes = importAll(require.context("../pages/notes/", true, /\.mdx$/), "/notes");
