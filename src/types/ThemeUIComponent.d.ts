import * as React from 'react'
import {SxProp} from 'theme-ui'

export interface ThemeUIProps extends SxProp {
    as?: React.ElementType
    variant?: string
}