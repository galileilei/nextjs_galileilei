import * as React from "react";
import { AppProps } from "next/app";
import { MDXProvider } from "@mdx-js/react";
import { ThemeProvider } from "theme-ui";
import theme from "../constants/theme";
import components from "../components/MDXComponents";
import Layout from "../components/Layout";

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <ThemeProvider theme={theme}>
      <MDXProvider components={components}>
        <Layout>
          <Component {...pageProps} />
          {/* follow this tutorial: https://codeconqueror.com/blog/using-google-fonts-with-next-js */}
          <style jsx global>{`
            @import url("https://fonts.googleapis.com/css2?family=Fira+Code&family=Lora&family=Open+Sans&display=swap");
          `}</style>
        </Layout>
      </MDXProvider>
    </ThemeProvider>
  );
};

export default App;
