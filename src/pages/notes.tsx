import * as React from "react";
import { notes, Project } from "../lib/utils";
import { Box, Grid } from "theme-ui";
import Card from "../components/Card";

const Projects: React.FC = () => {
  return (
    <React.Fragment>
      <Box mb={2}> Notes for future self. Maximum information density.</Box>
      <Grid columns={1}>
        {notes.map((post: Project) => {
          if (post.meta.show)
            return (<Card key={post.link} {...post} />)
        })}
      </Grid>
    </React.Fragment>
  );
};

export default Projects;