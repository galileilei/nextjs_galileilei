import * as React from "react";
import {projects, Project } from "../lib/utils";
import { Box, Grid } from "theme-ui";
import Card from "../components/Card";

const Projects: React.FC = () => {
  return (
    <React.Fragment>
      <Box mb={2}> It ain't much, but it is honest work.</Box>
      <Grid columns={[1, null, null, 2]}>
        {projects.map((post: Project) => {
          if (post.meta.show)
           return( <Card key={post.link} {...post} />)
          })}
      </Grid>
    </React.Fragment>
  );
};

export default Projects;
