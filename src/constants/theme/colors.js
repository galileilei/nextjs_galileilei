// import {
//   polaris,
//   system,
//   funk,
//   future,
//   roboto,
//   dark,
//   deep,
//   swiss,
//   tosh,
//   bootstrap,
// } from "@theme-ui/presets";

// const additionalMode = {
//   modes: {
//     polaris: polaris.colors,
//     system: system.colors,
//     funk: funk.colors,
//     future: future.colors,
//     roboto: roboto.colors,
//     dark: dark.colors,
//     deep: deep.colors,
//     swiss: swiss.colors,
//     tosh: tosh.colors,
//     bootstrap: bootstrap.colors,
//   },
// };

const systemMode = {
  // ...system.colors,
  // "colors": {
    "text": "hsl(210, 50%, 96%)",
    "background": "hsl(230, 25%, 18%)",
    "primary": "hsl(260, 100%, 80%)",
    "secondary": "hsl(290, 100%, 80%)",
    "highlight": "hsl(260, 20%, 40%)",
    "purple": "hsl(290, 100%, 80%)",
    "muted": "hsla(230, 20%, 0%, 20%)",
    "gray": "hsl(210, 50%, 60%)",
    modes: {
      dark: {
        "colors": {
          "text": "#fff",
          "background": "#060606",
          "primary": "#3cf",
          "secondary": "#e0f",
          "muted": "#191919",
          "highlight": "#29112c",
          "gray": "#999",
          "purple": "#c0f"
        },
      }
    }
  // }
}

export default systemMode;
