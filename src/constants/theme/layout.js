const layout = {
  resume: {
    maxWidth: "resume",
    borderStyle: "solid",
    borderWidth: [1, 2],
    p: 4,
    my: 4,
    margin: "auto",
  },
};

export default layout;