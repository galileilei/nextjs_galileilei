// styling Link
const styles = {
  a: {
    color: "text",

    "&:hover": {
      color: "secondary",
      borderColor: "secondary",
    },
  },
  card: {
    borderColor: "muted",
    borderStyle: "solid",
    borderWidth: 2,
    px: 4,
    py: 2,

    "&:hover": {
      borderColor: "secondary",
    },
  },
  pre: {
    fontSize: 2,
  },
  img: {
    borderRadius: "50%",
    height: 150,
    width: 150,
  },
};

export default styles;
