// https://theme-ui.com/components/nav-link/
/* NavLink variants can be defined in the theme.links object. By default the NavLink component will use styles defined in theme.links.nav. */
const links = {
  nav: {
    borderBottom: 2,
    borderBottomStyle: "solid",
    borderBottomColor: "muted",
    m: 2,
    p: 0,

    "&:hover": {
      color: "text",
      borderColor: "secondary",
    },

    "&[data-active]": {
      borderColor: "secondary",
    },
  },
};

export default links;
