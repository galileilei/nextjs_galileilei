const sizes = {
  container: "64rem",
  post: "54rem",
  resume: "45rem",
};

export default sizes;
