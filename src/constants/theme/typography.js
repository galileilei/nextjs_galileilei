// this is the default theme from system-ui
const fonts = {
  body:
    '"Lora", system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", sans-serif',
  heading: '"Open Sans", inherit',
  monospace: '"Fira Code", Menlo, monospace',
};
const fontSizes = [12, 14, 16, 20, 24, 32, 48, 64, 96];

const fontWeights = {
  body: 400,
  heading: 700,
  bold: 700,
};
const lineHeights = {
  body: 1.5,
  heading: 1.125,
};

export { fonts, fontSizes, fontWeights, lineHeights };
