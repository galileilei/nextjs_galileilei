import links from "./links";
import list from "./list";
import layout from "./layout";
import sizes from "./sizes";
import { fonts } from "./typography";
import colors from "./colors";
import buttons from "./buttons";
import styles from "./styles";
// import { merge } from "theme-ui";
// import { base } from "theme-ui/presets";

const myTheme = {
  fonts,
  links,
  list,
  sizes,
  layout,
  colors,
  buttons,
  styles,
};
export default myTheme;
// export default merge(base, myTheme);
