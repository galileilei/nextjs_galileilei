const buttons = {
  primary: {
    appreance: "none",
    fontFamily: "inherit",
    fontSize: 1,
    fontWeight: "bold",
    m: 0,
    color: "text",
    bg: "muted",
    border: 0,
    borderRadius: 2,
  },
};

export default buttons;
