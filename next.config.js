// next.config.js
const remarkMath = require("remark-math");
const remarkEmoji = require("remark-gemoji");
const remarkAutolink = require("remark-autolink-headings");

const rehypeKatex = require("rehype-katex");

// const withTM = require('next-transpile-modules')([
// 	'react-children-utilities',
// ])
const withMDX = require("@next/mdx")({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [remarkMath, remarkEmoji, remarkAutolink],
    // remarkPlugins: [],
    rehypePlugins: [rehypeKatex],
    // rehypePlugins: [],
  },
});

module.exports = withMDX({
  //withTM({
  pageExtensions: ["js", "jsx", "ts", "tsx", "md", "mdx"],
});
//}));
