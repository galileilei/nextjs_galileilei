# About

This is a pesonal homepage built with [Next.js][next-js-link] and [MDX][mdx-link]. I wanted:

- markdown + latex for 99% of the use cases, and custom React Components when needed;
- default theme for 99% of the use cases, and local style override when needed;
- rendered sensibly on desktop and all mobile devices;
- and most importantly, an excuse to use Typescript.

The combination, `Next.js + theme-ui + MDX`, fits the above constraints nicely.
   

[next-js-link]: https://github.com/vercel/next.js/
[mdx-link]: https://github.com/mdx-js 